self.addEventListener("install", e => {
  console.log("Install event ", e);
});
self.addEventListener("activate", e => {
  console.log("Activate event ", e);
  return self.clients.claim();
});

self.addEventListener("fetch", e => {
  console.log("Fetch e ", e);
  e.respondWith(fetch(e.request));
});
