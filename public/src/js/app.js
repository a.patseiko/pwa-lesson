var deferredPrompt;


if ("serviceWorker" in navigator) {
  navigator.serviceWorker.register("/sw.js").then(() => {
    console.log("servise worker register");
  });
}

window.addEventListener("beforeinstallprompt", e => {
  console.log("Before install fired");
  e.preventDefault();
  deferredPrompt = e;
  return false;
});
